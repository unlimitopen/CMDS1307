﻿/*
 * CMDS1307.c
 *
 * Created: 04.09.2016 09:16:27
 *  Author: Christian Möllers
 */

#include <avr/io.h>
#include "CMDS1307.h"
#include "../CMTWI/CMTwi.h"
#include "../CMUart0/CMUart0.h"

extern uint8_t sec;
extern uint8_t min;
extern uint8_t hour;
extern uint8_t date;
extern uint8_t month;
extern uint8_t tick;
extern uint8_t high;
extern uint8_t year;
extern uint8_t weekday;

uint8_t clockMode = hour24;
uint8_t clockwise = 0;

/**
	@brief	Initialisize the ds1307 with important parameters! 
			If you don't do that, the ic will not usable, because no second tick will
			work.
	@param 	none
	@return	none
*/
void initDS1307(void)
{
	uint8_t second = 0;
	uint8_t hour = 0;

	second = getSecondDS1307();
	second = decimalTobcdOutput(second);
	hour = getHourDS1307();
	hour = decimalTobcdOutput(hour);
	
	// if not, we need to define the CH (clock halt) bit
	if ( second & oscillatorinactive )
	{
		setOscillatorDS1307(oscillatoractive);
	}
	
	// we need that to read which status the register 0x02 (hour) and the bit6 and bit5 has
	// if the bit6 is high then 12 -hour mode is active 
	/*
	if ( hourMode & hourMode12 )
	{
		setHourModeDS1307(hourMode12);
	}
	else
	{
		setHourModeDS1307(hourMode24);
	}
	*/
}

/**
	@brief	set the IC oscillator on and of
	@param 	uint8_t set ( on 0x00 or off 0x80 ) 
	@return	none
*/
void setOscillatorDS1307(uint8_t set)
{
	uint8_t second = 0;
	second = getSecondDS1307();
	second = decimalTobcdOutput(second);
	i2cStart(DS1307Address | twWrite);
	i2cSend( secondAddress );
	
	if ( set & oscillatoractive )
	{
		i2cSend( second &~ set );
	}
	else
	{
		i2cSend( second | set );
	}
	i2cStop();
}

/**
	@brief	none
	@param 	none
	@return	none
*/
void setHourModeDS1307(uint8_t set)
{
	uint8_t hour = 0;
	hour = getHourDS1307();
	hour = decimalTobcdOutput(hour);
	i2cStart(DS1307Address | twWrite);
	i2cSend( hourAddress );
	hour = decimalTobcdOutput(hour);
	
	if ( set & hourMode12 )
	{
		i2cSend( hour | set );	
	}
	else
	{
		i2cSend( hour &~ set );
	}
	
	i2cStop();
}

/**
	@brief	none
	@param 	none
	@return	none
*/
void setSecondDS1307(uint8_t second)
{
	// BCD decode
	second = decimalTobcdOutput(second);

	i2cStart(DS1307Address | twWrite);
	i2cSend(secondAddress);
	i2cSend(second);
	i2cStop();
}

/**
	@brief	none
	@param 	none
	@return	none
*/
uint8_t getSecondDS1307(void)
{
	uint8_t second = 0;

	i2cStart(DS1307Address | twWrite);
	i2cSend(secondAddress);
	i2cStart(DS1307Address | twRead);
	second = i2cReceiveNack();
	i2cStop();

	// BCD encode
	second = bcdToDecimalOutput(second);

	return second;
}

/**
	@brief	set the second on ic to 0
	@param 	none
	@return	none
*/
void clearSecondDS1307(void)
{
	uint8_t second = 0;
	second = getSecondDS1307();
	second = decimalTobcdOutput(second);

	i2cStart(DS1307Address | twWrite);
	i2cSend( secondAddress );
	i2cSend( second & 0x80);
	i2cStop();
}

/**
	@brief	none
	@param 	none
	@return	none
*/
void setMinuteDS1307(uint8_t minute)
{
	// BCD decode
	minute = decimalTobcdOutput(minute);

	i2cStart(DS1307Address | twWrite);
	i2cSend(minuteAddress);
	i2cSend(minute);
	i2cStop();
}

/**
	@brief	none
	@param 	none
	@return	none
*/
uint8_t getMinuteDS1307(void)
{	
	uint8_t minute = 0;

	i2cStart(DS1307Address | twWrite);
	i2cSend(minuteAddress);
	i2cStop();
	i2cStart(DS1307Address | twRead);
	minute = i2cReceiveNack();
	i2cStop();
	
	// BCD encode
	minute = bcdToDecimalOutput(minute);

	return minute;
}

/**
	@brief	set the minute on ic to 0
	@param 	none
	@return	none
*/
void clearMinuteDS1307(void)
{
	uint8_t minute = 0;
	minute = getMinuteDS1307();
	minute = decimalTobcdOutput(minute);

	i2cStart(DS1307Address | twWrite);
	i2cSend( minuteAddress );
	i2cSend( minute & 0x7F);
	i2cStop();
}

/**
	@brief	none
	@param 	none
	@return	none
*/
void setHourDS1307(uint8_t hour)
{
	hour = decimalTobcdOutput(hour);

	// read the bit7 to have the hourMode for 24 or 12 o`clock.
	// read the bit6 to have the pm or am status.
	
	i2cStart(DS1307Address | twWrite);
	i2cSend(hourAddress);
	i2cSend(hour);
	i2cStop();
}

/**
	@brief	none
	@param 	none
	@return	none
*/
uint8_t getHourDS1307(void)
{
	uint8_t hour = 0;

	i2cStart(DS1307Address | twWrite);
	i2cSend(hourAddress);
	i2cStop();
	i2cStart(DS1307Address | twRead);
	hour = i2cReceiveNack();
	i2cStop();
	
	// read the bit6 to have the hourMode for 24 or 12 o`clock.
	if ( (hour & hourMode12) )
	{
		clockMode = hour12;
	}
	else
	{
		clockMode = hour24;
	}
	
	// read the bit6 to have the pm or am status. if the bit5 is high being PM
	if ( (hour & hourMode12) && (hour & hourModePM) )
	{
		clockwise = PM;
	}
	else
	{
		clockwise = AM;
	}

	// BCD encode
	hour = bcdToDecimalOutput(hour);

	return hour;
}

/**
	@brief	set the hour on ic to 0
	@param 	none
	@return	none
*/
void clearHourDS1307(void)
{
	uint8_t hour = 0;
	hour = getHourDS1307();
	hour = decimalTobcdOutput(hour);

	i2cStart(DS1307Address | twWrite);
	i2cSend( hourAddress );
	i2cSend( hour & 0x80);
	i2cStop();
}

/**
	@brief	none
	@param 	none
	@return	none
*/
void setWeekdayDS1307(uint8_t weekday)
{
	weekday = decimalTobcdOutput(weekday);

	i2cStart(DS1307Address | twWrite);
	i2cSend(weekdayAddress);
	i2cSend(weekday);
	i2cStop();
}

/**
	@brief	none
	@param 	none
	@return	none
*/
uint8_t getWeekdayDS1307(void)
{
	uint8_t weekday = 0;

	i2cStart(DS1307Address | twWrite);
	i2cSend(weekdayAddress);
	i2cStop();
	i2cStart(DS1307Address | twRead);
	weekday = i2cReceiveNack();
	i2cStop();

	// bcd to decimal
	weekday = bcdToDecimalOutput(weekday);

	return weekday;
}

/**
	@brief	set the year on ic to 0
	@param 	none
	@return	none
*/
void clearWeekdayDS1307(void)
{
	uint8_t weekday = 0;
	weekday = getWeekdayDS1307();
	weekday = decimalTobcdOutput(weekday);

	i2cStart(DS1307Address | twWrite);
	i2cSend( weekdayAddress );
	i2cSend( weekday & 0x80);
	i2cStop();
}

/**
	@brief	none
	@param 	none
	@return	none
*/
void setMonthDS1307(uint8_t month)
{
	month = decimalTobcdOutput(month);

	i2cStart(DS1307Address | twWrite);
	i2cSend(monthAddress);
	i2cSend(month);
	i2cStop();
}

/**
	@brief	none
	@param 	none
	@return	none
*/
uint8_t getMonthDS1307(void)
{
	uint8_t month = 0;

	i2cStart(DS1307Address | twWrite);
	i2cSend(monthAddress);
	i2cStop();
	i2cStart(DS1307Address | twRead);
	month = i2cReceiveNack();
	i2cStop();

	// bcd to decimal
	month = bcdToDecimalOutput(month);

	return month;
}

/**
	@brief	set the year on ic to 0
	@param 	none
	@return	none
*/
void clearMonthDS1307(void)
{
	uint8_t month = 0;
	month = getMonthDS1307();
	month = decimalTobcdOutput(month);

	i2cStart(DS1307Address | twWrite);
	i2cSend( monthAddress );
	i2cSend( month & 0x80);
	i2cStop();
}

/**
	@brief	none
	@param 	none
	@return	none
*/
void setyearDS1307(uint8_t year)
{
	year = decimalTobcdOutput(year);

	i2cStart(DS1307Address | twWrite);
	i2cSend(yearAddress);
	i2cSend(year);
	i2cStop();
}

/**
	@brief	none
	@param 	none
	@return	none
*/
uint8_t getyearDS1307(void)
{
	uint8_t year = 0;

	i2cStart(DS1307Address | twWrite);
	i2cSend(yearAddress);
	i2cStop();
	i2cStart(DS1307Address | twRead);
	year = i2cReceiveNack();
	i2cStop();

	// bcd to decimal
	year = bcdToDecimalOutput(year);

	return year;
}

/**
	@brief	set the year on ic to 0
	@param 	none
	@return	none
*/
void clearYearDS1307(void)
{
	uint8_t year = 0;
	year = getyearDS1307();
	year = decimalTobcdOutput(year);

	i2cStart(DS1307Address | twWrite);
	i2cSend( yearAddress );
	i2cSend( year & 0x80);
	i2cStop();
}

/**
	@brief	none
	@param 	none
	@return	none
*/
void setDateDS1307(uint8_t date)
{
	date = decimalTobcdOutput(date);

	i2cStart(DS1307Address | twWrite);
	i2cSend(dateAddress);
	i2cSend(date);
	i2cStop();
}

/**
	@brief	none
	@param 	none
	@return	none
*/
uint8_t getDateDS1307(void)
{
	uint8_t date = 0;

	i2cStart(DS1307Address | twWrite);
	i2cSend(dateAddress);
	i2cStop();
	i2cStart(DS1307Address | twRead);
	date = i2cReceiveNack();
	i2cStop();

	return date;
}

/**
	@brief	none
	@param 	none
	@return	none
*/
void clearDateDS1307(void)
{
	uint8_t date = 0;
	date = decimalTobcdOutput(date);
	i2cStart(DS1307Address | twWrite);
	i2cSend(dateAddress);
	i2cSend(date);
	i2cStop();
}

/**
	@brief	none
	@param 	none
	@return	none
*/
uint8_t getTimeDS1307(void)
{
	uint8_t time = 0;
	i2cStart(DS1307Address | twWrite);
	i2cSend(yearAddress);
	i2cStop();
	i2cStart(DS1307Address | twRead);
	time = i2cReceiveNack();
	i2cStop();

	return time;
}

/**
	@brief	none
	@param 	none
	@return	none
*/
void clearTimeDS1307(void)
{
	uint8_t time = 0;
	time = decimalTobcdOutput(time);
	i2cStart(DS1307Address | twWrite);
	i2cSend(yearAddress);
	i2cSend(time);
	i2cStop();
}

/**
	@brief	none
	@param 	none
	@return	none
*/
void setTimeVariablesFromDS1307(void)
{
	setWeekdayDS1307(weekday);
	setDateDS1307(date);
	setMonthDS1307(month);
	setyearDS1307(year);
	setHourDS1307(hour);
	setMinuteDS1307(min);
	setSecondDS1307(sec);
}

/**
	@brief	none
	@param 	none
	@return	none
*/
void getTimeVariablesFromDS1307(void)
{
	weekday = getWeekdayDS1307();
	date = getDateDS1307();
	month = getMonthDS1307();
	year = getyearDS1307();
	hour = getHourDS1307();
	min = getMinuteDS1307();
	sec = getSecondDS1307();
}

/**
	@brief	this is needed for bcd input and decimal ouput
	@param 	decimal
	@return	uint8_t decimal 
*/
uint8_t bcdToDecimalOutput(uint8_t decimal)
{
	decimal = ((decimal / 16) * 10) + (decimal % 16);
	return decimal;
}

/**
	@brief	this is needed for decimal input and bcd ouput
	@param 	bcd
	@return	uint8_t bcd 
*/
uint8_t decimalTobcdOutput(uint8_t bcd)
{
	bcd = ((bcd / 10) * 16) + (bcd % 10);
	return bcd;
}
